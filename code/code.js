/*Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий *
 Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

/*const div = document.createElement("div");
document.body.prepend(div); 
console.log(getComputedStyle(div).margin);*/
const btnStart = document.getElementById("start"), 
btnStop = document.getElementById("stop"), 
sec = document.getElementById("sec"),
btnReset = document.getElementById("reset");
const stopwatch = document.querySelector(".container-stopwatch");

let second = 0;

btnStop.onclick = () =>{
    clearClass();
    stopwatch.classList.add("red");
    
};
btnStart.onclick = () =>{
    clearClass();
    stopwatch.classList.add("green"); 
};

btnReset.onclick = () =>{
    clearClass();
    stopwatch.classList.add("silver"); 
}
function clearClass () {
    stopwatch.classList.remove("red");
    stopwatch.classList.remove("black");
    stopwatch.classList.remove("green");
    stopwatch.classList.remove("silver");
}
setInterval(()=>{
    second++;
    showInterval();

}, 1000);

function showInterval (){
sec.innerText = second;    

}
